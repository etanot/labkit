package grpccorrelation

import "context"

// grpcHealthCheckMethodFullName is the name of the standard GRPC health check full method name.
const grpcHealthCheckMethodFullName = "/grpc.health.v1.Health/Check"

// healthCheckFilterFunc will exclude all GRPC health checks from tracing
// since these calls are high frequency, but low value from a tracing point
// of view, excluding them reduces the tracing load.
func healthCheckFilterFunc(_ context.Context, fullMethodName string) bool {
	return fullMethodName != grpcHealthCheckMethodFullName
}
