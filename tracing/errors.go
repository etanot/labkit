package tracing

import (
	"fmt"
)

// ErrConfiguration is returned when the tracer is not properly configured.
var ErrConfiguration = fmt.Errorf("tracing is not properly configured")
