package correlation

// FieldName is the field used in structured logs that represents the correlationID.
const FieldName = "correlation_id"
