package correlation

import (
	"crypto/rand"
	"io"
	"sync"
	"time"

	"github.com/oklog/ulid/v2"
)

// Replaceable for testing purposes.
var ulidEntropySource io.Reader = &safeMonotonicReader{
	delegate: ulid.Monotonic(rand.Reader, 0),
}

func generatePseudorandomCorrelationID() string {
	return "E:" + encodeReverseBase62(time.Now().UnixNano())
}

// generateRandomCorrelationID will attempt to generate a correlationid randomly
// if this fails, will log a message and fallback to a pseudorandom approach.
func generateRandomCorrelationIDWithFallback() string {
	uid, err := ulid.New(ulid.Timestamp(time.Now()), ulidEntropySource)
	if err != nil {
		// Swallow the error and return a pseudorandom correlation_id.
		// Operators can determine that an error occurred by the shape of the
		// correlation_id, which will be prefixed with a `E:`
		return generatePseudorandomCorrelationID()
	}

	return uid.String()
}

// RandomID generates a random correlation ID.
// Deprecated: use SafeRandomID instead.
// Note, that this method will not return an error, it is here for compatibility reasons only.
func RandomID() (string, error) { return generateRandomCorrelationIDWithFallback(), nil }

// SafeRandomID generates a random correlation ID.
func SafeRandomID() string { return generateRandomCorrelationIDWithFallback() }

// safeMonotonicReader is a thread-safe wrapper around a ulid.Monotonic instance, which is not safe for concurrent use by itself.
// See https://godoc.org/github.com/oklog/ulid#Monotonic.
type safeMonotonicReader struct {
	mtx      sync.Mutex
	delegate ulid.MonotonicReader
}

var _ ulid.MonotonicReader = &safeMonotonicReader{}

func (r *safeMonotonicReader) MonotonicRead(ms uint64, p []byte) error {
	r.mtx.Lock()
	defer r.mtx.Unlock()
	return r.delegate.MonotonicRead(ms, p)
}

func (r *safeMonotonicReader) Read(p []byte) (int, error) {
	r.mtx.Lock()
	defer r.mtx.Unlock()
	return r.delegate.Read(p)
}
